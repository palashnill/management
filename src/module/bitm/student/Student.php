<?php

namespace app\module\bitm\student;
class Student
{
    //Two Properties
    public $course_name;
    public $batch;

    //construct function
    public function __construct($course_name = '', $batch = '')
    {
        $this->course_name = $course_name;
        $this->batch = $batch;
    }


    //Two Methods
    public function setName()
    {

        return 'Web Application Development-<b>' . $this->course_name . '</b> Batch : <b>' . $this->batch . '</b>';

    }

    public function getName()
    {
        echo $this->setName();
    }
}

class StudentManagement extends Student
{
    public function subClassStd()
    {
        echo $this->getName();

    }
}
